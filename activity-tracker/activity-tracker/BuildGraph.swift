//
//  BuildGraph.swift
//  activity-tracker
//
//  Created by Nathan Rockell on 19/12/2021.
//

import Foundation
import SwiftUI
import UIKit
import Charts
import TinyConstraints
import CoreMotion

class BuildGraph{
    //Variables
    var AXValues = [ChartDataEntry]()
    var AYValues = [ChartDataEntry]()
    var AZValues = [ChartDataEntry]()
    var AXArray: [Double] = []
    var AYArray: [Double] = []
    var AZArray: [Double] = []
    var setAX: LineChartDataSet = LineChartDataSet()
    var setAY: LineChartDataSet = LineChartDataSet()
    var setAZ: LineChartDataSet = LineChartDataSet()
    let motion = CMMotionManager()
    var recordTimer : Timer?
    var ai = 0.0;
    
    func startRecording(){
        print("Recording Started")
        motion.startAccelerometerUpdates()
        AXArray = []
        AYArray = []
        AZArray = []
        
        recordTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: true) { [self] _ in
            if let dataA = self.motion.accelerometerData{
                let ax = dataA.acceleration.x
                let ay = dataA.acceleration.y
                let az = dataA.acceleration.z
                AXArray.append(ax)
                AYArray.append(ay)
                AZArray.append(az)
                AXValues.append(ChartDataEntry(x: Double(ai), y:ax))
                AYValues.append(ChartDataEntry(x: Double(ai), y:ay))
                AZValues.append(ChartDataEntry(x: Double(ai), y:az))
                ai+=0.25
            }
        }
    }
    
    func stopRecording(){
        print("Recording Stopped")
        ai = 0.0
        motion.stopAccelerometerUpdates()
        recordTimer?.invalidate()
        
        setAX = LineChartDataSet(entries: AXValues, label: "AccelX")
        setAX.highlightColor = .red
        setAX.setColor(ChartColorTemplates.colorFromString("#ff0000"))
        
        setAY = LineChartDataSet(entries: AYValues, label: "AccelY")
        setAY.highlightColor = .blue
        setAY.setColor(ChartColorTemplates.colorFromString("#00ff00"))
        
        setAZ = LineChartDataSet(entries: AZValues, label: "AccelZ")
        setAZ.highlightColor = .green
        setAZ.setColor(ChartColorTemplates.colorFromString("#0000ff"))
    }
}
