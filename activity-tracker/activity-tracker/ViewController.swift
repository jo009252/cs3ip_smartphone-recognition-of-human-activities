//
//  ViewController.swift
//  activity-tracker
//
//  Created by Nathan Rockell on 29/10/2021.
//

import UIKit
import CoreMotion
import Charts
import TinyConstraints

class ViewController: UIViewController, ChartViewDelegate {
    //Variables
    
    @IBOutlet weak var recordButton: UIButton!
    var isChecked = false
    var recordTimer : Timer?
    let b = BuildGraph()
    
    lazy var lineChartView: LineChartView = {
        let chartView = LineChartView()
        chartView.backgroundColor = .systemGray2
        return chartView
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lineChartView.delegate = self
        view.addSubview(lineChartView)
        self.lineChartView.top(to: view)
        self.lineChartView.width(to: view)
        self.lineChartView.heightToWidth(of: view)

    }
    
    @IBAction func recordData(_ sender: UIButton){
        isChecked = !isChecked
        if isChecked {
            b.startRecording()
            recordButton.tintColor = UIColor.red
            
        }else{
            b.stopRecording()
            recordButton.tintColor = UIColor.green
            let activityTextBoxAlert = UIAlertController(title: "Activity Performed", message: "Enter The Activity Performed.", preferredStyle: .alert)
            activityTextBoxAlert.addTextField{(textField) in
                textField.placeholder = "Activity"
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let saveAction = UIAlertAction(title: "Save", style: .default) { _ in

                // this code runs when the user hits the "save" button

                let input = activityTextBoxAlert.textFields![0].text
                let data = LineChartData(dataSets: [self.b.setAX, self.b.setAY, self.b.setAZ])
                self.lineChartView.data = data
                self.lineChartView.notifyDataSetChanged()
                
                self.writeToFile(array: self.b.AXArray, fileName: "\(input ?? "unknown")_x.csv")
                self.writeToFile(array: self.b.AYArray, fileName: "\(input ?? "unknown")_y.csv")
                self.writeToFile(array: self.b.AZArray, fileName: "\(input ?? "unknown")_z.csv")

            }
            activityTextBoxAlert.addAction(cancelAction)
            activityTextBoxAlert.addAction(saveAction)

            present(activityTextBoxAlert, animated: true, completion: nil)
        }
        
    }
    
    func writeToFile(array: [Double], fileName: String){
        let file = fileName
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(file)
            let stringArray = array.map { String($0) }
            let string = stringArray.joined(separator: "\n")
            do {
                try string.write(to: fileURL, atomically: false, encoding: String.Encoding.utf8)
                print("Writing to file")
            }
            catch {
                print("Failed to write to file")
            }
        }
    }
    
}
