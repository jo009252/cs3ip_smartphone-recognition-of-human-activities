#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 15:51:13 2022

@author: nathanrockell
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, ifft, fftfreq

#88.5 Seconds Long
accel_x = np.loadtxt("/Users/nathanrockell/Blink Creative Media Dropbox/Nathan Rockell/3. Education/4. Degree/Reading University/3rd Year/CS3IP - Project/AccelerometerData/WalkingAXArray.txt", dtype=float)
accel_y = np.loadtxt("/Users/nathanrockell/Blink Creative Media Dropbox/Nathan Rockell/3. Education/4. Degree/Reading University/3rd Year/CS3IP - Project/AccelerometerData/WalkingAYArray.txt", dtype=float)
accel_z = np.loadtxt("/Users/nathanrockell/Blink Creative Media Dropbox/Nathan Rockell/3. Education/4. Degree/Reading University/3rd Year/CS3IP - Project/AccelerometerData/WalkingAZArray.txt", dtype=float)

print(len(accel_x))



def runFFT(x, y, z):
    N = len(accel_x)
    T = 244
    
    t = np.linspace(0, T, N)

    
    f = fftfreq(len(t), np.diff(t)[0])
    
    xfft = fft(x)
    
    yfft = fft(y)
    
    zfft = fft(z) 
    
    plt.plot(t, x, label="Accel X")
    plt.plot(t, y, label="Accel Y")
    plt.plot(t, z, label="Accel Z")
    plt.legend(loc="upper right")
    plt.xlabel('t [seconds]')
    plt.ylabel('Accelerometer Value')
    plt.show()
    
    
    plt.plot(f[:N//2], np.abs(xfft[:N//2]), label ="Accel X FFT")
    plt.legend(loc="upper right")
    plt.show()
    plt.plot(f[:N//2], np.abs(yfft[:N//2]), label ="Accel Y FFT")
    plt.legend(loc="upper right")
    plt.show()
    plt.plot(f[:N//2], np.abs(zfft[:N//2]), label ="Accel Z FFT")
    plt.legend(loc="upper right")
    plt.show()
    
runFFT(accel_x, accel_y, accel_z)






